# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/yogeshwardancharan/Android/Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

###################################################################################################
# ButterKnife generates and uses  classes dynamically which means static analysis tools like      #
# proguard may think they are unused .In order to prevent them from being removed ,explicitly     #
# mark them to be kept . To prevent ProGuard renaming classes that use @Bing on a member field the#
# keepclasseswithmembernames option is used .                                                     #
# For more info see : http://jakewharton.github.io/butterknife/                                   #
###################################################################################################
-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keep class **$$ViewBinder { *; }

-keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}

-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}

-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions
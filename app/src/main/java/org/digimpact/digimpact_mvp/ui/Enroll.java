package org.digimpact.digimpact_mvp.ui;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.digimpact.digimpact_mvp.R;
import org.digimpact.digimpact_mvp.application.ApplicationController;
import org.digimpact.digimpact_mvp.model.EnrolleeModel;
import org.digimpact.digimpact_mvp.util.Constants;
import org.digimpact.digimpact_mvp.util.GeoLocation;
import org.digimpact.digimpact_mvp.util.retrofit.PhotoUploadService;
import org.digimpact.digimpact_mvp.util.retrofit.ServiceGenerator;
import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import timber.log.Timber;


/*
* Activity for enrollment of a new client .
*
* */
public class Enroll extends AppCompatActivity  implements ResultCallback<LocationSettingsResult> {

    @Bind(R.id.enroll_layout_root)protected View mLayout ;

    private static final String TAG = "Enroll";

    /*Constant for Location Permission Request.
    * */
    private static final int REQUEST_LOCATION = 0x0;

    /**
     * Constant used in the location settings dialog.
     */
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;

    //Todo: back button in Action bar not working.

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;


    private static final int REQUEST_IMAGE_CAPTURE = 0x2;

    private static final int CHOOSE_PHOTO = 0x3;

    private static final int REQUEST_STORAGE = 0x4;

    private EditText firstNameEditText, lastNameEditText;
    private String firstName = "",lastName=""; //get values in onResume()
    private Character gender = null;
    private DatePicker dateOfBirthDatePicker = null;
    private String dateOfBirth = null;

    private Bitmap imageBitmap = null;


    @Bind(R.id.enrolee_photo_IV) ImageView mEnroleePhotoImageView ;
    @Bind(R.id.take_photo_button) Button takePhotoButton ;
    @Bind(R.id.location_text_view) TextView locationTextView;


    ProgressDialog uploadProgressDialog;

    GoogleApiClient mGoogleApiClient = null;
    RadioGroup genderRadioGroup = null;

    GeoLocation geoLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enroll);
        //Bind view using annotation using butterknife library
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);


        setSupportActionBar(toolbar);



        //Todo:[ENROLL] check for NullPointerException.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        /* Initialize mGoogleAPIClient to get location coordinates
         * Also , create Location Settings Dialog giving option to turn GPS on if it's not already
         * so .
         */
        if(checkPlayServices()) {
            geoLocation = new GeoLocation(this);
            mGoogleApiClient = geoLocation.googleApiClient;
            geoLocation.createLocationRequest();
            geoLocation.buildLocationSettingsRequest();
        }


        //logic for date handling below

        //get a date formatter
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

        dateOfBirthDatePicker = (DatePicker)findViewById(R.id.dob_date_picker);
        dateOfBirthDatePicker.setMaxDate(new Date().getTime());
        Calendar calendar = Calendar.getInstance();
        //initialize dateOfBirth to currentDate i.e. today's date
        Date currentDate = new Date(calendar.get(Calendar.YEAR)-1900, calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        //dateOfBirth = simpleDateFormat.format(currentDate);

        dateOfBirthDatePicker.init(calendar.get(Calendar.YEAR)-1, calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {

            /**
             * Called upon a date change.
             *
             * @param datePicker        The view associated with this listener.
             * @param year        The year that was set.
             * @param month The month that was set (0-11) for compatibility
             *                    with {@link Calendar}.
             * @param dayOfMonth  The day of the month that was set.
             */
            @Override
            public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {


                /*
                * Please read this as to why {year-1900} is done below
                * Link : http://stackoverflow.com/questions/33562828/creating-a-new-date-in-java-using-ints
                * We'll use LocalDate when Android supports JAVA 8.
                * */
                Date date = new Date(year-1900,month,dayOfMonth);
                dateOfBirth = simpleDateFormat.format(date);

            }
        });




        //handle radio group for gender selection
        genderRadioGroup = (RadioGroup)findViewById(R.id.gender_RG);
        genderRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.male_RB) {
                    gender = 'M';
                } else {
                    gender = 'F';
                }

            }
        });

        //initialize editTexts
        firstNameEditText = (EditText)findViewById(R.id.first_name_ET);
        lastNameEditText = (EditText)findViewById(R.id.last_name_ET);


    }

    protected void onStart() {
        if(mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        super.onStart();
    }

    @Override
    protected void onResume(){


        // Within {@code onPause()}, we pause location updates, but leave the
        // connection to GoogleApiClient intact.  Here, we resume receiving
        // location updates .

        super.onResume();
        Timber.d("checkLocationsetings() called!");
        /*
        * Before allowing user to proceed to fill form we must ensure that user has got
        * his GPS turned on so that we can get location .
        *
        * If GPS is not turned on we show a dialog to turn on GPS settings.
        *
        * */

        checkLocationSettings();
        Timber.d("checkLocationSettings() ends!");
        if (checkPlayServices() && mGoogleApiClient.isConnected() ) {
            Timber.d("onResume : calling getLocation()");
            geoLocation.getLocation();
            geoLocation.startLocationUpdates();
        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient.isConnected()) {
            geoLocation.stopLocationUpdates();
        }
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }




    //to enroll a person and save data
    @OnClick(R.id.enroll_button)
    public void enrollButtonHandler(View v) {


        firstName = firstNameEditText.getText().toString().trim();
        lastName = lastNameEditText.getText().toString().trim();
        if(isInputValid()) {

           // Toast.makeText(Enroll.this, "Name: "+firstName+" "+lastName+"\nGender: "+gender+"\nDOB:"+dateOfBirth+"\nLocation:"+geoLocation.location+"\nfile:"+mCurrentPhotoPath,Toast.LENGTH_SHORT).show();
            /**
             * As a STEP 1. of POSTing data to server we first upload photo using Retrofit Asynchronously
             *
             * Once photo is uploaded successfully , in STEP 2. : we POST other data which includes
             *
             * among other field in form the filename of image POSTed using Retrofit to associate
             *
             *it with this Data sent via Volley Asynchronously.
             *
             *
             *
             *
             */

            //Start showing progress Dialog here at beginning of STEP 1 i.e. POST photo using Retrofit
            // and dismiss it after STEP 2 i.e. POST data using Volley.
            Timber.d("Creating And showing Progress Dialog!");
            createAndShowProgressDialog();
            Timber.d("calling uploadFile(photoUri)");
            uploadFile(photoUri);

        }
    }

    private boolean isInputValid() {


        if(firstName == null || firstName.equals("")){
            Toast.makeText(Enroll.this, "First name can't be empty!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(lastName == null || lastName.equals("")){
            Toast.makeText(Enroll.this, "Last name can't be empty!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(gender == null ){
            Toast.makeText(Enroll.this, "Select Gender please!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(dateOfBirth == null){
            //will never occur since dateOfBirth has been initialized properly to current date
            Toast.makeText(Enroll.this, "Null or Invalid date of birth!", Toast.LENGTH_SHORT).show();
            return false;
        }



        //location check

        if(geoLocation.mLastLocation == null){
            Toast.makeText(Enroll.this, "Location not fetched properly.", Toast.LENGTH_SHORT).show();
            return false;
        }

        //image check : if bitmap encoded to string is null , either image not taken or encoding not
        //done properly
        if(imageBitmap == null){
            Toast.makeText(Enroll.this, "Please take photo!", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }




    /**
     * Method to verify google play services on the device
     * */
    private boolean checkPlayServices() {

        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }







    /*
                * Make post request to Enroll API using volley.
                * Todo: Use second answer here for multiple scenario generic solution :http://stackoverflow.com/questions/23220695/send-post-request-with-json-data-using-volley
                *
                *
                * Blog for volley used in code:http://arnab.ch/blog/2013/08/asynchronous-http-requests-in-android-using-volley/
                * */
    public void  sendUserDataUsingVolley(){q
        final String enrollApiURL = "http://52.39.179.170:8080/digdataservice/enroll";


        // Post params to be sent to the server
        Timber.d("Creating params to POST to server.");
        HashMap<String, String> params = new HashMap<String, String>();

        params.put("name","facePhoto");
        params.put("firstName",firstName);
        params.put("lastName",lastName);
        params.put("dob",dateOfBirth);
        params.put("gender",gender.toString());
        params.put("longitude",String.valueOf(geoLocation.longitude));
        params.put("latitude", String.valueOf(geoLocation.latitude));
        params.put("file",mCurrentPhotoPath);






        JsonObjectRequest req = new JsonObjectRequest(enrollApiURL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //Toast.makeText(Enroll.this, "Date sent Successfully ..Enrollment Successful!", Toast.LENGTH_SHORT).show();

                            VolleyLog.v("Response:%n %s", response.toString(4));

                            //create EnrolleModel for this enrolled person to be sent as parcealable to
                            // Display PersonProfile
                            String id = response.getString("id");

                            EnrolleeModel person = new EnrolleeModel(id,lastName,dateOfBirth,
                                    String.valueOf(geoLocation.latitude) ,gender.toString(),
                                    String.valueOf(geoLocation.longitude),firstName);


                            Timber.d("Creating personParcel and personBundle!");

                            Parcelable personParcel = Parcels.wrap(person);
                            Bundle personBundle = new Bundle();
                            personBundle.putParcelable("person_parcel", personParcel);


                            //dismiss Progress Dialog
                            Timber.d("Dismiss uploadProgressDialog : Success!");
                            uploadProgressDialog.dismiss();

                            Intent launchCapture = new Intent(Enroll.this,org.digimpact.digimpact_mvp.ui.irishield.IriShieldDemo.class);
                            launchCapture.putExtra("id",id);
                            launchCapture.putExtra("parent_activity","Enroll");
                            launchCapture.putExtra("person_bundle",personBundle);
                            launchCapture.putExtra("person_image", photoUri.toString());


                            Timber.d("Starting Capture Activity!");
                            startActivity(launchCapture);
                            finish();

                           // Toast.makeText(Enroll.this, id , Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Timber.d("Dismiss uploadProgressDialog : Failure!");

                uploadProgressDialog.dismiss();
                Toast.makeText(Enroll.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                VolleyLog.e("Error: ", error.getMessage());


            }
        });

// add the request object to the queue to be executed
        ApplicationController.getInstance().addToRequestQueue(req);




    }



    /**
     * Check if the device's location settings are adequate for the app's needs using the
     * {@link com.google.android.gms.location.SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} method, with the results provided through a {@code PendingResult}.
     */
    public void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        geoLocation.mLocationSettingsRequest
                );

        result.setResultCallback(Enroll.this);
    }







    /**
     * The callback invoked when
     * {@link com.google.android.gms.location.SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} is called. Examines the
     * {@link com.google.android.gms.location.LocationSettingsResult} object and determines if
     * location settings are adequate. If they are not, begins the process of presenting a location
     * settings dialog to the user.
     */
    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Timber.i("All location settings are satisfied.");
                Timber.d("onResult : calling getLocation -> startLocationUpdate()");
                geoLocation.getLocation();
                geoLocation.startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Timber.i("Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(Enroll.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Timber.i("PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Timber.i("Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            /*
            * case to handle response of user to GPS settings dialog.
            * */
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Timber.i("User agreed to make required location settings changes.");
                        Timber.d("onActivityResult : calling getLocation ->  startLocationUpdate");
                        geoLocation.getLocation();
                        geoLocation.startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        Timber.i("User chose not to make required location settings changes.");
                        break;
                }
                break;
            /*
            * Case to handle response from Camera App regarding face photo of Enrollee.
            * */
            case REQUEST_IMAGE_CAPTURE:
                switch (resultCode){
                    case RESULT_OK:
                        Timber.d("Image capture successful!");

                        Timber.d("URI printing : "+photoUri.toString());
                        //since we are passing URI for EXTRA_OUTPUT we'll get image from there
                        // and not from Intent data which will be null

                        try {
                            imageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), photoUri);
                            mEnroleePhotoImageView.setImageBitmap(imageBitmap);
                        }catch (IOException e){
                            e.printStackTrace();
                            Toast.makeText(Enroll.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                        break;

                }
                break;
            /*
            * case to handle response from gallery for Choose Photo button :
            * dispatchChoosePictureIntent
            * */
            case CHOOSE_PHOTO:
                switch (resultCode){
                    case RESULT_OK:

                        if(data!=null) {

                            int columnIndex = -1; //random initialization just to avoid error
                            Timber.d("Choosing photo successful!");

                           // photoUri = data.getData();
                            String[] filePathColumn = { MediaStore.Images.Media.DATA };
                            Cursor cursor = getContentResolver().query(data.getData(), filePathColumn, null, null, null);
                            try {
                                cursor.moveToFirst();
                                columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                //mCurrentPhotoPath = cursor.getString(columnIndex);


                                File selectedImage = new File(cursor.getString(columnIndex));
                                photoUri = Uri.fromFile(selectedImage);
                                imageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), photoUri);
                                mCurrentPhotoPath = "file:" + selectedImage.getAbsolutePath();

                                cursor.close();
                            }catch (NullPointerException | IOException e){
                                e.printStackTrace();
                            }

                            mEnroleePhotoImageView.setImageBitmap(imageBitmap);

                        }else{
                            Timber.d("Data == null ,Choosing photo failed!");
                        }

                        break;
                    case RESULT_CANCELED:
                        Timber.d("Choosing photo failed!");
                        break;




                }
                break;
        }
    }

    /*
    * Fucntion to request Runtime Permissions in Android MarshMallow(i.e. API >=23 Version>=6)
    * */
    public void requestLocationPermission(){

        Timber.d("Location permission wasn't granted ,requesting for Location permission.");
        if(ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION)||
                ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_COARSE_LOCATION)){
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            Timber.d("Diplaying Location Permission rationale if permission not granted earlier.");
            Snackbar.make(mLayout,R.string.location_permission_rationale,Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(Enroll.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    REQUEST_LOCATION);

                            ActivityCompat.requestPermissions(Enroll.this,
                                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                                    REQUEST_LOCATION);
                        }
                    })
                    .show();

        }
        else {

            /*
            * Location Permission hasn't been denied earlier i.e. it's first time ,so , request directly.
            * */
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
            ActivityCompat.requestPermissions(Enroll.this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_LOCATION);
        }
    }
    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults){
        if(requestCode == REQUEST_LOCATION){
            //recieved response for Location Permission
            //check if requested permissions were granted
            if(grantResults.length == 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Timber.d("onRequestPermissionsResult: Location permission granted , calling geoLocation.getLocation().");
                // We can now safely use the API we requested access to
                geoLocation.getLocation();
                geoLocation.startLocationUpdates();
            } else {
                // Permission was denied or request was cancelled
                Timber.d("Permision was denied or request was cancelled.");
            }
        }else if(requestCode == REQUEST_STORAGE){
            if(grantResults.length == 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Timber.d("Storage Permission granted!");

            }else{
                Timber.d("Storage Permission not granted !");
                Toast.makeText(Enroll.this, "Please grant storage permission for saving photo.", Toast.LENGTH_SHORT).show();
            }

        }


    }

    /*
    * Method to delegate task fo capturing image by call to Camera App via Intent.
    *
    * Called when Take Photo button is clicked.
    * */

    Uri photoUri;

    @OnClick(R.id.take_photo_button)
     public void dispatchTakePictureIntent(View view) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //there must  be a camera app to resolve this activity
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                // Error occurred while creating the File
                e.printStackTrace();

            }
            // Continue only if the File was successfully created
            photoUri = Uri.fromFile(photoFile);
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }


    /*
    * Method to select photo from gallery using intent , result is handled on
    * onActivityResult()
    *
    **/


    @OnClick(R.id.choose_photo_button)
    void dispatchChoosePictureIntent(View view){
        Intent choosePhotoIntent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
       // choosePhotoIntent.setAction();
       // choosePhotoIntent.setType("image/*");
        startActivityForResult(choosePhotoIntent, CHOOSE_PHOTO);
    }


    /*
    * Display location in location_text_view
    * */
    @OnClick(R.id.get_location_button)
    void locationButtonHandler(View View){

        locationTextView.setText(geoLocation.location);


    }

    void createAndShowProgressDialog(){
        //ProgressDialog to show when data is being uploaded(POST) to server
        uploadProgressDialog= new ProgressDialog(this);
        uploadProgressDialog.setTitle("Enroll client...");
        uploadProgressDialog.setMessage("Uploading data...");
        uploadProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        uploadProgressDialog.setCancelable(false);
        uploadProgressDialog.setIcon(R.mipmap.ic_launcher);
        uploadProgressDialog.show();
    }

    private void uploadFile(Uri fileUri) {

        //instantiate ServiceGenerator


        ServiceGenerator serviceGenerator = new ServiceGenerator(Constants.BASE_API_URL);

        // create upload service client


        PhotoUploadService service =
                serviceGenerator.createService(PhotoUploadService.class);

        //get the actual file by uri
        File file = new File(fileUri.getPath());

        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);


        // finally, execute the request asynchronously i.e. enqueue
        Call<String> call = service.upload(mCurrentPhotoPath,requestFile);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {

                Timber.d("Response code : %d", response.code());
                Timber.d("Response body : \n%s" ,response.body());
                if (response.isSuccessful()) {
                    Timber.d("Image upload done , sending data now..called sendUserDataUsingVolley() ");
                   // Toast.makeText(Enroll.this, "Image upload done , sending data now...", Toast.LENGTH_SHORT).show();
                    sendUserDataUsingVolley();
                } else {
                    uploadProgressDialog.dismiss();
                    Timber.d("In onResponse : Image upload failed, aborting enrollment...");
                    Toast.makeText(Enroll.this, "In onResponse : Image upload failed, aborting enrollment...", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                uploadProgressDialog.dismiss();
                t.printStackTrace();
                Timber.d(t.getMessage());
                Timber.d("Image upload failed, aborting enrollment...");
                Toast.makeText(Enroll.this, "Image upload failed, aborting enrollment...", Toast.LENGTH_SHORT).show();
            }
        });
    }

    String mCurrentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";

        File storageDir = null;

        if(isStoragePermissionGranted()) {

            storageDir = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES);
        }
        else{
            Timber.d("Suitable Storage location not available... return null.");
            Toast.makeText(Enroll.this, "Suitable Storage location not available.", Toast.LENGTH_SHORT).show();
            return null;
        }
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        Timber.d("Image File created!");
        Timber.d("Photo path : %s",mCurrentPhotoPath);
        return image;
    }


    /*
    * Method to handle runtime permission check for External Storage , so that photo can be stored
    * in it .
    *
    * */
    public boolean isStoragePermissionGranted(){
        if(Build.VERSION.SDK_INT>=23){
            if(ActivityCompat.checkSelfPermission(this,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED){
                Timber.d("Storage Permssion is already granted to app!");

                return true;
            }
            else{
                Timber.d("Storage Permission isn't granted , requesting for permission!");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE);
                Timber.d("Permission Requested!");
                return false;
            }


        }
        else{
            Timber.d("Device SDK <= 23,no runtime permission required , permissions were already granted!");
            return true;
        }
    }
}

package org.digimpact.digimpact_mvp.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;

import org.digimpact.digimpact_mvp.R;
import org.digimpact.digimpact_mvp.adapter.PersonListAdapter;
import org.digimpact.digimpact_mvp.model.EnrolleeModel;
import org.digimpact.digimpact_mvp.ui.irishield.IriShieldDemo;
import org.digimpact.digimpact_mvp.util.Constants;
import org.digimpact.digimpact_mvp.util.retrofit.GetEnrolleesService;
import org.digimpact.digimpact_mvp.util.retrofit.ServiceGenerator;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class Search extends AppCompatActivity  implements SearchView.OnQueryTextListener {

    private Call<List<EnrolleeModel>> getEnrolleeListCall = null ;
    private RecyclerView mRecyclerView;
    private PersonListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<EnrolleeModel> mArrayList;
    List<EnrolleeModel> mEnrolleeModelList;
    private SearchView mSearchView;
    private String mSearchText = "";

    static final String SEARCH = "search";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //instantiating ServiceGenerator

        ServiceGenerator serviceGenerator = new ServiceGenerator(Constants.BASE_API_URL);

        GetEnrolleesService getEnrolleesService = serviceGenerator.createService(GetEnrolleesService.class);

        getEnrolleeListCall = getEnrolleesService.getUsers();
        mRecyclerView = (RecyclerView) findViewById(R.id.list_people);
        mArrayList = new ArrayList<>();
        mEnrolleeModelList = new ArrayList<>();
        mAdapter = new PersonListAdapter(mArrayList);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setmOnItemClickListener(new PersonListAdapter.SetOnClickListener(){
            @Override
            public void onItemClick(int position, View itemView) {
                EnrolleeModel person = (EnrolleeModel) mAdapter.getItem(position);
                Timber.d("Item clicked : %s",person.getId());
                Intent irisVerificationLauncher = new Intent(Search.this, IriShieldDemo.class);
                irisVerificationLauncher.putExtra("id",person.getId());
                irisVerificationLauncher.putExtra("parent_activity","Search");
                startActivity(irisVerificationLauncher);


            }
        });
        if (savedInstanceState != null && savedInstanceState.getString(SEARCH) != null) {
            mSearchText = savedInstanceState.getString(SEARCH);
        }
    }

    @Override
    protected void onResume(){
        super.onResume();

        getEnrolleeListCall.clone().enqueue(new Callback<List<EnrolleeModel>>() {
            @Override
            public void onResponse(Call<List<EnrolleeModel>> call, Response<List<EnrolleeModel>> response) {
                Timber.d("Response successful, code : %d", response.code());

                mEnrolleeModelList = response.body();
                for(EnrolleeModel enrolleeModel : mEnrolleeModelList){
                    mArrayList.add(enrolleeModel);
                }
                mAdapter.notifyDataSetChanged();

                //Timber.d(enrolleeModelList.toString());
                Timber.d("Response size : %d",mEnrolleeModelList.size());

            }

            @Override
            public void onFailure(Call<List<EnrolleeModel>> call, Throwable t) {

                t.printStackTrace();
                Timber.d("Response failed");

            }
        });

    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        if (mSearchView != null) {
            bundle.putString(SEARCH, mSearchText);
        }
        super.onSaveInstanceState(bundle);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_search, menu);
        mSearchView = (SearchView) menu.findItem(R.id.action_search_person).getActionView();
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setQuery(mSearchText, false);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        ArrayList<EnrolleeModel> fileteredArrayList = new ArrayList<>();
        if(!TextUtils.isEmpty(query)) {
            for(EnrolleeModel person: mEnrolleeModelList) {
                if(person.getFirstName().contains(query) || person.getLastName().contains(query)){
                    fileteredArrayList.add(person);
                }
            }
            mAdapter.animateTo(fileteredArrayList);
        }
        else {
            mAdapter.animateTo(mEnrolleeModelList);
        }
        mSearchText = query;
        return false;
    }

}

package org.digimpact.digimpact_mvp.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import org.digimpact.digimpact_mvp.R;

public class SurveyFormNames extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_form_names);

        Button survey1= (Button) findViewById(R.id.survey1);
        Button survey2= (Button) findViewById(R.id.survey2);
        Button survey3= (Button) findViewById(R.id.survey3);
        Button survey4= (Button) findViewById(R.id.survey4);

        survey1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getApplicationContext(),Forms.class);
                startActivity(i);
            }
        });

        survey2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getApplicationContext(),Forms.class);
                startActivity(i);
            }
        });

        survey3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getApplicationContext(),Forms.class);
                startActivity(i);
            }
        });
        survey4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getApplicationContext(),Forms.class);
                startActivity(i);
            }
        });
    }
}

package org.digimpact.digimpact_mvp.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import org.digimpact.digimpact_mvp.R;

import java.util.Locale;

public class ChooseLocale extends AppCompatActivity implements View.OnClickListener {
    Button hiButton;
    Button enButton;
    Button mrButton;
    Locale mLocale;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_locale);
        hiButton = (Button) findViewById(R.id.button_hi);
        enButton = (Button) findViewById(R.id.button_en);
        mrButton = (Button)findViewById(R.id.button_mr);
        hiButton.setOnClickListener(this);
        enButton.setOnClickListener(this);
        mrButton.setOnClickListener(this);

    }
    public void changeLang(String lang)
    {
        if (lang.equalsIgnoreCase(""))
            return;
        mLocale = new Locale(lang);
        Locale.setDefault(mLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = mLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }
    @Override
    public void onClick(View v) {
        String lang = "en";
        switch (v.getId()) {
            case R.id.button_en:
                lang = "en";
                break;
            case R.id.button_hi:
                lang = "hi";
                break;
            case R.id.button_mr:
                lang = "mr";
                break;
            default:
                break;
        }
        changeLang(lang);
        Intent intent = new Intent(this, Home.class);
        startActivity(intent);
    }
    @Override
    public void onConfigurationChanged(android.content.res.Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mLocale != null){
            newConfig.locale = mLocale;
            Locale.setDefault(mLocale);
            getBaseContext().getResources().updateConfiguration(newConfig, getBaseContext().getResources().getDisplayMetrics());
        }
    }

}

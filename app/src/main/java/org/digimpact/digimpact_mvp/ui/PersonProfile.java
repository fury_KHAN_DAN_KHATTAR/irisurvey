package org.digimpact.digimpact_mvp.ui;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.digimpact.digimpact_mvp.R;
import org.digimpact.digimpact_mvp.model.EnrolleeModel;
import org.parceler.Parcels;

import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PersonProfile extends AppCompatActivity implements OnMapReadyCallback {


    @Bind(R.id.name_edit_view) TextView nameEditView ;

    @Bind(R.id.gender_text_view) TextView genderTextView ;

    @Bind(R.id.longitude_text_view) TextView longitudeTextView ;

    @Bind(R.id.latitude_text_view) TextView latitudeTextView;

    @Bind(R.id.dob_text_view) TextView dobTextView ;

    @Bind(R.id.person_image) ImageView personImageView;


    EnrolleeModel person;
    private Bitmap personImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);


        //get Person data
        Bundle personBundle = getIntent().getBundleExtra("person_bundle");

        person = Parcels.unwrap(personBundle.getParcelable("person_parcel"));
        Uri photoUri = Uri.parse(getIntent().getStringExtra("person_image"));
        try {
            personImage = MediaStore.Images.Media.getBitmap(getContentResolver(), photoUri);
        }catch (IOException e){
            e.printStackTrace();
        }

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.location_map);
        mapFragment.getMapAsync(this);


    }

    @Override protected void onResume(){
        super.onResume();
        createProfileUi();



    }


    @Override
    public void onMapReady(GoogleMap map) {
        Double lat = Double.valueOf(person.getLattitude());
        Double lon = Double.valueOf(person.getLongitude());
        LatLng latLng = new LatLng(lat,lon);
        map.addMarker(new MarkerOptions()
                .position(latLng)
                .title("Marker"));
        map.moveCamera((CameraUpdateFactory.newLatLngZoom(latLng, 15)));
    }

    public void createProfileUi(){
        nameEditView.setEnabled(false);
        nameEditView.setText(person.getFirstName() + " " + person.getLastName());
        genderTextView.setText("Gender : "+person.getGender());
        latitudeTextView.setText("Lattitude : "+person.getLattitude());
        longitudeTextView.setText("Longitude : "+person.getLongitude());
        dobTextView.setText("D.O.B. : "+person.getDob());
        personImageView.setImageBitmap(personImage);


    }

}

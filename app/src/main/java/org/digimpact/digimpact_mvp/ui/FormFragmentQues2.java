package org.digimpact.digimpact_mvp.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import org.digimpact.digimpact_mvp.R;

/**
 * Created by toothless on 9/4/16.
 */
public class FormFragmentQues2 extends Fragment implements Forms.OnPageChangeListener {

    protected CheckBox cb1;
    protected CheckBox cb2;
    protected CheckBox cb3;
    protected CheckBox cb4;
    protected CheckBox cb5;
    protected boolean isQuesFilled=false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_form_ques2,null);
        cb1= (CheckBox) v.findViewById(R.id.cb1);
        cb2= (CheckBox) v.findViewById(R.id.cb2);
        cb3= (CheckBox) v.findViewById(R.id.cb3);
        cb4= (CheckBox) v.findViewById(R.id.cb4);
        cb5= (CheckBox) v.findViewById(R.id.cb5);

        Forms.setOnPageChangeListener2(this);

        return v;
    }

    public boolean isFormFilledCompletely(){
        return isQuesFilled;
    }

    @Override
    public boolean onPageSelected(int position) {
        if(position==2){
            if(cb1.isChecked()){
                isQuesFilled=true;
            }else if (cb2.isChecked()){
                isQuesFilled=true;
            }else if (cb3.isChecked()){
                isQuesFilled=true;
            }else if (cb4.isChecked()){
                isQuesFilled=true;
            }else if (cb5.isChecked()){
                isQuesFilled=true;
            }
        }
        return isQuesFilled;
    }
}

package org.digimpact.digimpact_mvp.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import org.digimpact.digimpact_mvp.R;

/**
 * Created by toothless on 9/4/16.
 */
public class FormFragmentQues1 extends Fragment implements Forms.OnPageChangeListener {

    private CheckBox cb1;
    private CheckBox cb2;
    private CheckBox cb3;
    private CheckBox cb4;
    private boolean isFirstQuesFilled=false;
    private boolean isSecondQuesFilled=false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_form_ques1,null);
        cb1= (CheckBox) v.findViewById(R.id.cb1);
        cb2= (CheckBox) v.findViewById(R.id.cb2);
        cb3= (CheckBox) v.findViewById(R.id.cb3);
        cb4= (CheckBox) v.findViewById(R.id.cb4);

        Forms.setOnPageChangeListener1(this);


        return v;
    }

    public boolean isFormFilledCompletely(){
        if(isFirstQuesFilled && isSecondQuesFilled){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public boolean onPageSelected(int position) {
        if(position==1){
            if(cb1.isChecked()){
                isFirstQuesFilled=true;
            }else if(cb2.isChecked()){
                isFirstQuesFilled=true;
            }

            if(cb3.isChecked()){
                isSecondQuesFilled=true;
            }else  if (cb4.isChecked()){
                isSecondQuesFilled=true;
            }
        }
        boolean output= isFirstQuesFilled && isSecondQuesFilled;
        return output;
    }

}

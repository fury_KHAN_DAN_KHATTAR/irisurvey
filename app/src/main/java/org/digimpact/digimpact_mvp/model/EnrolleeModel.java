package org.digimpact.digimpact_mvp.model;

import org.parceler.Parcel;

/**
 * Created by yogeshwardancharan on 9/4/16.
 */


/*
* Class representing Enrollee Model .
*
* Here's it's being used as a sample POJO for Retrofit for API response we get from /users API .
*
* In response ,/users return  a JSONArray  .
*
*
* */

@Parcel
public class EnrolleeModel {

    public String id;

    public String lastName;

    public String dob;

    public String lattitude;

    public String gender;

    public String longitude;

    public String firstName;

    public EnrolleeModel(String id, String lastName, String dob, String lattitude, String gender,
                         String longitude, String firstName) {
        this.id = id;
        this.lastName = lastName;
        this.dob = dob;
        this.lattitude = lattitude;
        this.gender = gender;
        this.longitude = longitude;
        this.firstName = firstName;
    }


    //required empty constructor
    public EnrolleeModel(){

    }



    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getLastName ()
    {
        return lastName;
    }

    public void setLastName (String lastName)
    {
        this.lastName = lastName;
    }

    public String getDob ()
    {
        return dob;
    }

    public void setDob (String dob)
    {
        this.dob = dob;
    }

    public String getLattitude ()
    {
        return lattitude;
    }

    public void setLattitude (String lattitude)
    {
        this.lattitude = lattitude;
    }

    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

    public String getLongitude ()
    {
        return longitude;
    }

    public void setLongitude (String longitude)
    {
        this.longitude = longitude;
    }

    public String getFirstName ()
    {
        return firstName;
    }

    public void setFirstName (String firstName)
    {
        this.firstName = firstName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", lastName = "+lastName+", dob = "+dob+", lattitude = "+lattitude+", gender = "+gender+", longitude = "+longitude+", firstName = "+firstName+"]";
    }
}

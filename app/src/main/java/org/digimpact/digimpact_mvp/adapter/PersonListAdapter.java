package org.digimpact.digimpact_mvp.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;

import org.digimpact.digimpact_mvp.R;
import org.digimpact.digimpact_mvp.model.EnrolleeModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by suheb on 9/4/16.
 */
public class PersonListAdapter extends RecyclerView.Adapter<PersonListAdapter.ViewHolder> {
    private List<EnrolleeModel> mDataset;
    private ViewHolder.SetOnClickListener mOnItemClickListener;


    public interface OnItemClickListener{
        void onItemClick(View v,int position);
    }

    public void setmOnItemClickListener(ViewHolder.SetOnClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }

    public static final int[] COLORS = {Color.BLUE, Color.GRAY, Color.RED, Color.MAGENTA};

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView mTextView;

        private ImageView mImageView;

        private SetOnClickListener mItemClickListenerListener;

        public interface SetOnClickListener {
            void onItemClick(int position, View itemView);
        }

        public TextView getmTextView() {
            return mTextView;
        }

        public void setmTextView(TextView mTextView) {
            this.mTextView = mTextView;
        }

        public ImageView getmImageView() {
            return mImageView;
        }

        public void setmImageView(ImageView mImageView) {
            this.mImageView = mImageView;
        }

        public SetOnClickListener getmItemClickListenerListener() {
            return mItemClickListenerListener;
        }

        public void setmItemClickListenerListener(SetOnClickListener listener) {
            this.mItemClickListenerListener = listener;
        }

        public ViewHolder(View view) {
            super(view);
            view.setClickable(true);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListenerListener.onItemClick(getAdapterPosition(), view);
                }
            });
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public PersonListAdapter(List<EnrolleeModel> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PersonListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_person, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.setmTextView((TextView) view.findViewById(R.id.person_name));
        viewHolder.setmImageView((ImageView) view.findViewById(R.id.person_image));

        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element1
        EnrolleeModel person = mDataset.get(position);
        holder.getmTextView().setText(person.getFirstName() + " " + person.getLastName());

        TextDrawable drawable = TextDrawable.builder()
                .buildRound(person.getFirstName().substring(0,1), COLORS[position%4]);
        holder.getmImageView().setImageDrawable(drawable);
        holder.setmItemClickListenerListener(mOnItemClickListener);
    }
    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    /**
     * Animates addition/removal of items whenever the data changes.
     * Use this method when replacing data
     */
    public void animateTo(List<EnrolleeModel> newData) {
        applyAndAnimateRemovals(newData);
        applyAndAnimateAdditions(newData);
        applyAndAnimateMovedItems(newData);
    }

    private void applyAndAnimateRemovals(List<EnrolleeModel> newData) {
        List<EnrolleeModel> dataset = new ArrayList<>(this.mDataset);
        for (int i = dataset.size() - 1; i >= 0; i--) {
            final EnrolleeModel data = dataset.get(i);
            if (!newData.contains(data)) {
                removeItem(this.mDataset.indexOf(data));
            }
        }
    }

    private void applyAndAnimateAdditions(List<EnrolleeModel> newData) {
        List<EnrolleeModel> dataset = new ArrayList<>(this.mDataset);
        for (int i = 0, count = newData.size(); i < count; i++) {
            final EnrolleeModel data = newData.get(i);
            if (!dataset.contains(data)) {
                addItem(i, data);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<EnrolleeModel> newData) {
        List<EnrolleeModel> dataset = new ArrayList<>(this.mDataset);
        for (int toPosition = newData.size() - 1; toPosition >= 0; toPosition--) {
            final EnrolleeModel data = newData.get(toPosition);
            final int fromPosition = dataset.indexOf(data);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }
    public void addItem(int position, EnrolleeModel data) {
        mDataset.add(position, data);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final EnrolleeModel data = mDataset.remove(fromPosition);
        mDataset.add(toPosition, data);
        notifyItemMoved(fromPosition, toPosition);
    }
    public EnrolleeModel removeItem(int position) {
        final EnrolleeModel speaker = mDataset.remove(position);
        notifyItemRemoved(position);
        return speaker;
    }
    public EnrolleeModel getItem(int position) {
        return mDataset.get(position);
    }
    public interface SetOnClickListener extends ViewHolder.SetOnClickListener {
        void onItemClick(int position, View itemView);
    }
}
  
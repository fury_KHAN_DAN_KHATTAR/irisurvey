package org.digimpact.digimpact_mvp.util.retrofit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

/**
 * Created by yogeshwardancharan on 15/3/16.
 */


/*
* This class is used to generate REST API client with a given API Base Url.
*
* */
public class ServiceGenerator {

    private String API_BASE_URL = null;

    /*
    * @constructor
    * @params : API_BASE_URL : Base url of API , must end with '/' "
    * */

    public ServiceGenerator(String baseUrl){
        API_BASE_URL = baseUrl;

    }

    private  OkHttpClient.Builder httpClient = new OkHttpClient.Builder();



    /*
    * Method to instantiate a new Service .
    * The serviceClass defines the annotated class or interface for API requests
    * */
    public  <S> S createService(Class<S> serviceClass) {


        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(API_BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create());


        Retrofit retrofit = builder.client(httpClient.build()).build();

        return retrofit.create(serviceClass);
    }
}

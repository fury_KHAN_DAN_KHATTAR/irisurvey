package org.digimpact.digimpact_mvp.util.retrofit;

import org.digimpact.digimpact_mvp.model.EnrolleeModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by yogeshwardancharan on 9/4/16.
 */


public interface GetEnrolleesService {

    @GET("users")
    Call<List<EnrolleeModel>> getUsers();
}

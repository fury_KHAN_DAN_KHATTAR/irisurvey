package org.digimpact.digimpact_mvp.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.digimpact.digimpact_mvp.ui.Enroll;

import timber.log.Timber;

/**
 * Created by yogeshwardancharan on 15/2/16.
 */

/*
* Class for getting GeoLocation of client .
* Constructor returns a GoogleApiClient whose LocationServices API is used .
*
* Currently we're fetching location using getLastLocation() from Google Play Service this requires
 * wifi and some app which fetched last location.
* For testing purpose , please enable wifi , location and open google map if location is null.
*
* Todo: [GeoLocation.java] : See how you can get location coordinates withou wifi i.e
* either use Android Platform API or see how to use Google Play Service Location API offline.
 * Also , currently location is obtained through getLastLocation() , change it so that your app
 * gets location as soon as it opens rather than relying on some other app to call for location.
*
*
* */
public class GeoLocation implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,LocationListener
{
    private Enroll enrollInstance;

    private static final String TAG = GeoLocation.class.getSimpleName();


    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 600000; // 10 minutes

    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    public LocationRequest mLocationRequest;

    /**
     * Stores the types of location services the client is interested in using. Used for checking
     * settings to determine if the device has optimal location settings.
     */
    public LocationSettingsRequest mLocationSettingsRequest;




    Context context;
    public Location mLastLocation = null;
    public double latitude ;
    public double longitude ;
    public String location = "";
    public GoogleApiClient googleApiClient = null;


    /*
    * Constructor
    * @param Context
    *
    * Create an instance of GoogleAPIClient and add  LocationService API to it for use.
    * */
    public GeoLocation(Context context){

        this.context = context;
        enrollInstance = (Enroll)context;


        //TODO:Eliminate this if condtion since,hopefully googleApiClient will always be null.
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(context)
                    .addConnectionCallbacks(GeoLocation.this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {


        // Connected to Google Play services!
        // Get last location using google play location service .

        if (locationRuntimePermissionsNotGranted()) {
            /* Handle case when user don't grant location permission etc here.Probably ,
            * this is case of Runtime Permissions on Android M.
            */
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            //for ActivityCompat#requestPermissions for more details.
            //

            Timber.d("Calling reuqestLocationPermission.");
            enrollInstance.requestLocationPermission();
            //Todo : show snackbar showing reason why you need location permission

        }

        else{
            Timber.d("Location is already granted.");
            //getLocation();
            //startLocationUpdates();
        }





    }

    public void getLocation(){
        Timber.d("Getting location using getLastLocation.");

        /*
        * Since this function is only called once in onConnected() callback to get location for first
        * time , and we're explicitly checking for user granting Location Permission , hence we
        * don't need to check for permissions here again . Below warning will not result in error.
        *
        *
        *
        * */
        if(locationRuntimePermissionsNotGranted()) {
            Timber.d("Calling reuqestLocationPermission.");
            enrollInstance.requestLocationPermission();

        }
        else{
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        }
        if (mLastLocation != null) {

            latitude = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();

            location = "Lat: "+ String.valueOf(latitude) +"  Lon: " +String.valueOf(longitude);

            //Toast.makeText(context, location, Toast.LENGTH_SHORT).show();
        }
        else{

           // Toast.makeText(context,"Location not yet successfully fetched!", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection has been interrupted.
        // Disable any UI components that depend on Google APIs
        // until onConnected() is called.
        //Todo:Don't allow Enroll if connection is suspended.
        googleApiClient.connect();//this will trigger onConnected() callback when connection to Google Play services is established

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // This callback is important for handling errors that
        // may occur while attempting to connect with Google.
        //
        // More about this in the 'Handle Connection Failures' section.
        String connectionFailureMessage = connectionResult.getErrorMessage();

        Log.d(TAG, connectionFailureMessage);
        Toast.makeText(context, connectionFailureMessage, Toast.LENGTH_SHORT).show();

    }

    /**
     * Check if Location settings turned off by user .
     *
     * If Location setting is turned off then both location provider i.e. GPS and NETWORK will be
     * disabled.
     *
     * Todo:We'll display a prompt aksing user to turn ON Location if isLocationEnabled() returns false.
     * */



    public void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Uses a {@link com.google.android.gms.location.LocationSettingsRequest.Builder} to build
     * a {@link com.google.android.gms.location.LocationSettingsRequest} that is used for checking
     * if a device has the needed location settings.
     */
    public void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        mLocationSettingsRequest = builder.build();
    }

    /**
     * Requests location updates from the FusedLocationApi.
     */
    public void startLocationUpdates() {

        Timber.d("startLocationUpdates() start..Location Permission check for Android M.");

        // The final argument to {@code requestLocationUpdates()} is a LocationListener
        // (http://developer.android.com/reference/com/google/android/gms/location/LocationListener.html).
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){

            Timber.d("Location Permission not Granted on Android M");
            Timber.d("Calling reuqestLocationPermission.");
            enrollInstance.requestLocationPermission();

        }
        else{

            Timber.d("Locations are already granted .... calling requestLocationUpdates()");
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    googleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        /*
        * if we've got correct location for current place , ping no further and stopLocationUpdate.
        * */
        if(location != null){
            stopLocationUpdates();


        mLastLocation = location;
        latitude = mLastLocation.getLatitude();
        longitude = mLastLocation.getLongitude();

        this.location = "Lat: "+ String.valueOf(latitude) +"  Lon: " +String.valueOf(longitude);
            //Toast.makeText(context, this.location, Toast.LENGTH_SHORT).show();
        }


    }

    public void stopLocationUpdates() {
        if (googleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    googleApiClient, this);
        }
    }


    public boolean locationRuntimePermissionsNotGranted(){
        return (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED);
    }


}

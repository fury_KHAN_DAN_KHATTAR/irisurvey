package org.digimpact.digimpact_mvp.util.retrofit;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by yogeshwardancharan on 14/3/16.
 */
public interface PhotoUploadService {

    @Multipart
    @POST("upload")
    Call<String> upload(@Part("name") String name,@Part("file\";filename=\"photo.jpg\"") RequestBody file);
}
